# Coursework dsa

In this project, developed a system that neatly encapsulates and manages the core details of each student enrolled in the  school. We considered a fixed number of students to be enrolled.

DATA STRUCTURE USED: Array data structure

CONSIDERATIONS:
1. Arrays provide a straight-forward  way to store and manage a fixed size collection of elements.

2. Arrays alllow easy access to elements in this case students which is useful for quikly retrieving, updating or deleting records.

3. Arrays offer a compact memory layout, with contiguous storage for elements which can lead to better cache performance and memory utilization.


LINKS TO THE 3 MINUTES VIDEOS:
KETRA : https://youtu.be/QhtBK_aKYMc?si=Hrx3hi7gkLkBdT2E

ANNET: https://youtu.be/pcIk9Xw3Xaw?si=PiObd26-p_udAW0P

AFUA:https://youtu.be/2Me9lPuO5tA?si=J3JJwFwqClvA30P1

ALBERT: https://youtu.be/7AaPYj73Rys?si=Gnmddpl2oYw-GYXZ

ALINDA: https://youtu.be/TNiQNLHkhu0


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME_LENGTH 51 // 50 characters + null terminator
#define DATE_LENGTH 11     // YYYY-MM-DD + null terminator
#define REG_NUM_LENGTH 7   // 6 characters + null terminator
#define PROGRAM_CODE_LENGTH 5 // 4 characters + null terminator

// Define the structure for a student
struct Student {
    char name[MAX_NAME_LENGTH];
    char dateOfBirth[DATE_LENGTH];
    char registrationNumber[REG_NUM_LENGTH];
    char programCode[PROGRAM_CODE_LENGTH];
    float annualTuition;
};

void createStudent(struct Student *s) {
    printf("Enter student name: ");
    scanf("%50[^\n]%*c", s->name); // Read up to 50 characters until newline, then consume newline
    printf("Enter date of birth (YYYY-MM-DD): ");
    scanf("%10s%*c", s->dateOfBirth); // Read up to 10 characters until newline, then consume newline
    printf("Enter registration number: ");
    scanf("%6s%*c", s->registrationNumber); // Read up to 6 characters until newline, then consume newline
    printf("Enter program code: ");
    scanf("%4s%*c", s->programCode); // Read up to 4 characters until newline, then consume newline
    printf("Enter annual tuition: ");
    scanf("%f%*c", &s->annualTuition); // Read float until newline, then consume newline
}

void displayStudent(struct Student s) {
    printf("Name: %s", s.name);
    printf("Date of Birth: %s", s.dateOfBirth);
    printf("Registration Number: %s", s.registrationNumber);
    printf("Program Code: %s", s.programCode);
    printf("Annual Tuition: %.2f\n", s.annualTuition);
}

void searchByRegistrationNumber(struct Student *students, int numStudents, char *regNum) {
    // Trim any trailing newline characters from the input registration number
    regNum[strcspn(regNum, "\n")] = '\0';

    int found = 0;
    for (int i = 0; i < numStudents; i++) {
        if (strcmp(students[i].registrationNumber, regNum) == 0) {
            found = 1;
            printf("Student found:\n");
            displayStudent(students[i]);
            return;
        }
    }
    if (!found) {
        printf("Student with registration number %s not found.\n", regNum);
    }
}

void deleteStudentByRegistrationNumber(struct Student *students, int *numStudents, char *regNum) {
    // Trim any trailing newline characters from the input registration number
    regNum[strcspn(regNum, "\n")] = '\0';

    int found = 0;
    for (int i = 0; i < *numStudents; i++) {
        if (strcmp(students[i].registrationNumber, regNum) == 0) {
            found = 1;
            // Move all subsequent students one position back in the array
            for (int j = i; j < *numStudents - 1; j++) {
                students[j] = students[j + 1];
            }
            (*numStudents)--;
            printf("Student with registration number %s deleted.\n", regNum);
            return;
        }
    }
    if (!found) {
        printf("Student with registration number %s not found.\n", regNum);
    }
}

void updateStudentByRegistrationNumber(struct Student *students, int numStudents, char *regNum) {
    // Trim any trailing newline characters from the input registration number
    regNum[strcspn(regNum, "\n")] = '\0';

    int found = 0;
    for (int i = 0; i < numStudents; i++) {
        if (strcmp(students[i].registrationNumber, regNum) == 0) {
            found = 1;
            printf("Enter updated details for student with registration number %s:\n", regNum);
            createStudent(&students[i]); // Use createStudent to update details
            printf("Student details updated successfully.\n");
            return;
        }
    }
    if (!found) {
        printf("Student with registration number %s not found.\n", regNum);
    }
}

// Helper function for sorting by name
int compareByName(const void *a, const void *b) {
    return strcmp(((struct Student *)a)->name, ((struct Student *)b)->name);
}

// Helper function for sorting by annual tuition
int compareByTuition(const void *a, const void *b) {
    return ((struct Student *)a)->annualTuition - ((struct Student *)b)->annualTuition;
}

void sortStudents(struct Student *students, int numStudents, int sortBy) {
    switch (sortBy) {
        case 1:
            qsort(students, numStudents, sizeof(struct Student), compareByName);
            break;
        case 2:
            qsort(students, numStudents, sizeof(struct Student), compareByTuition);
            break;
        default:
            printf("Invalid option.\n");
            break;
    }
}

void exportToCSV(struct Student *students, int numStudents) {
    FILE *fp;
    fp = fopen("student_records.csv", "a");
    if (fp == NULL) {
        printf("Error opening file.\n");
        return;
    }
    for (int i = 0; i < numStudents; i++) {
        fprintf(fp, "%s,%s,%s,%s,%.2f\n", students[i].name, students[i].dateOfBirth, 
                                           students[i].registrationNumber, students[i].programCode,
                                           students[i].annualTuition);
    }
    fclose(fp);
    printf("Records exported to student_records.csv successfully.\n");
}

int main() {
    int option;
    int numStudents = 0;
    struct Student students[100]; // Assuming a maximum of 100 students
    
    do {
        printf("\nMenu Options:\n");
        printf("1. Add a student\n");
        printf("2. Display all students\n");
        printf("3. Search student by registration number\n");
        printf("4. Delete student by registration number\n");
        printf("5. Update student by registration number\n");
        printf("6. Sort students\n");
        printf("7. Export student records to CSV\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &option);
        getchar(); // consume newline character
        
        switch (option) {
            case 1:
                printf("\nEnter details for new student:\n");
                createStudent(&students[numStudents]);
                numStudents++;
                break;
            case 2:
                printf("\nAll Students:\n");
                for (int i = 0; i < numStudents; i++) {
                    displayStudent(students[i]);
                }
                break;
            case 3: {
                char regNum[REG_NUM_LENGTH];
                printf("\nEnter registration number to search: ");
                fgets(regNum, REG_NUM_LENGTH, stdin);
                searchByRegistrationNumber(students, numStudents, regNum);
                break;
            }
            case 4: {
                char regNum[REG_NUM_LENGTH];
                printf("\nEnter registration number to delete: ");
                fgets(regNum, REG_NUM_LENGTH, stdin);
                deleteStudentByRegistrationNumber(students, &numStudents, regNum);
                break;
            }
            case 5: {
                char regNum[REG_NUM_LENGTH];
                printf("\nEnter registration number to update: ");
                fgets(regNum, REG_NUM_LENGTH, stdin);
                updateStudentByRegistrationNumber(students, numStudents, regNum);
                break;
            }
            case 6: {
                int sortBy;
                printf("\nSort Options:\n");
                printf("1. Sort by name\n");
                printf("2. Sort by annual tuition\n");
                printf("Enter your choice: ");
                scanf("%d", &sortBy);
                sortStudents(students, numStudents, sortBy);
                printf("\nSorted Students:\n");
                for (int i = 0; i < numStudents; i++) {
                    displayStudent(students[i]);
                }
                break;
            }
            case 7:
                exportToCSV(students, numStudents);
                break;
            case 8:
                printf("Exiting program.\n");
                break;
            default:
                printf("Invalid option. Please try again.\n");
                break;
        }
    } while (option != 8);

    return 0;
}